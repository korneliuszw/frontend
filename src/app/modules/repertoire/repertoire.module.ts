import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RepertoireDomainModule} from '../../domains/repertoire/repertoire.module';
import {RepertoireComponent} from './components/repertoire/repertoire.component';
import {RepertoireItemComponent} from './components/repertoire/compontents/repertoire-item/repertoire-item.component';
import {AppRoutingModule} from '../../app-routing.module';

@NgModule({
  declarations: [RepertoireComponent, RepertoireItemComponent],
  imports: [
    CommonModule,
    RepertoireDomainModule,
    AppRoutingModule
  ],
  exports: [RepertoireComponent]
})
export class RepertoireModule {
}
