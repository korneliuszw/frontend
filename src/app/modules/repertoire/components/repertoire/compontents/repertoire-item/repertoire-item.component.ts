import {Component, Input, OnInit} from '@angular/core';
import {RepertoireMovieData} from '../../../../../../domains/repertoire/interfaces/repertoire';

@Component({
  selector: 'app-repertoire-item',
  templateUrl: './repertoire-item.component.html',
  styleUrls: ['./repertoire-item.component.scss']
})
export class RepertoireItemComponent implements OnInit {

  @Input() movie: RepertoireMovieData;

  constructor() {
  }

  ngOnInit(): void {
  }

  getShortContent(value: string): string {
    const shortContent = value.substring(0, 200);
    const lastDot = shortContent.lastIndexOf('.');

    return shortContent.substring(0, lastDot + 1);
  }

}
