import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../ngrx/app.state';
import {RepertoireMovieData} from '../../../../domains/repertoire/interfaces/repertoire';
import {selectRepertoireItems} from '../../../../domains/repertoire/ngrx/selectors';

@Component({
  selector: 'app-repertoire',
  templateUrl: './repertoire.component.html',
  styleUrls: ['./repertoire.component.scss']
})
export class RepertoireComponent implements OnInit {

  repertoire: RepertoireMovieData[];

  constructor(
    private store: Store<AppState>
  ) {
  }

  ngOnInit() {
    this.store.pipe(select(selectRepertoireItems))
      .subscribe(data => this.repertoire = data);
  }
}
