import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './components/list/list.component';
import {MovieDomainModule} from '../../domains/movie/movie-domain.module';
import {MovieListItemComponent} from './components/list/components/movie-list-item/movie-list-item.component';

@NgModule({
  declarations: [ListComponent, MovieListItemComponent],
  imports: [
    CommonModule,
    MovieDomainModule
  ],
  exports: [ListComponent]
})
export class MoviesListModule {
}
