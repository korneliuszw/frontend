import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {tap} from 'rxjs/operators';
import {Movie} from '../../../../domains/movie/interfaces/movie';
import {AppState} from '../../../../ngrx/app.state';
import {selectMoviesItems} from '../../../../domains/movie/ngrx/selectors';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  movies: Movie[];

  constructor(
    private store: Store<AppState>
  ) {
  }

  ngOnInit() {
    this.store.select(selectMoviesItems)
      .pipe(
        tap(data => console.log(data))
      )
      .subscribe(data => this.movies = data);
  }

}
