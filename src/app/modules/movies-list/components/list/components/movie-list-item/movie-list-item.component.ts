import {Component, Input, OnInit} from '@angular/core';
import {Movie} from '../../../../../../domains/movie/interfaces/movie';

@Component({
  selector: 'app-movie-list-item',
  templateUrl: './movie-list-item.component.html',
  styleUrls: ['./movie-list-item.component.scss']
})
export class MovieListItemComponent implements OnInit {

  @Input() movie: Movie;

  constructor() {
  }

  ngOnInit(): void {
  }

  getShortContent(value: string): string {
    const shortContent = value.substring(0, 200);
    const lastDot = shortContent.lastIndexOf('.');

    return shortContent.substring(0, lastDot + 1);
  }

}
