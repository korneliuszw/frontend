import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyTicketsDomainModule} from '../../domains/my-tickets/my-tickets.module';
import {MyTicketsComponent} from './components/my-tickets/my-tickets.component';
import { TicketComponent } from './components/my-tickets/components/ticket/ticket.component';


@NgModule({
  declarations: [MyTicketsComponent, TicketComponent],
  imports: [
    CommonModule,
    MyTicketsDomainModule
  ],
  exports: [MyTicketsComponent]
})
export class MyTicketsModule {
}
