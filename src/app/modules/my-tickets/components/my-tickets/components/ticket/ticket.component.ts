import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MyTicketData} from '../../../../../../domains/my-tickets/ngrx/interface';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {

  @Input() ticket: MyTicketData;

  @Output() onDeleteTicket = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  handleDeleteTicket(ticket: MyTicketData): void {
    this.onDeleteTicket.emit(ticket);
  }

}
