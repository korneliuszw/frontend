import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {selectMyTicketsItems} from '../../../../domains/my-tickets/ngrx/selectors';
import {AppState} from '../../../../ngrx/app.state';
import {map, switchMap, tap} from 'rxjs/operators';
import {selectRoomItems} from '../../../../domains/room/ngrx/selectors';
import {selectMoviesItems} from '../../../../domains/movie/ngrx/selectors';
import {MyTicket, MyTicketData} from '../../../../domains/my-tickets/ngrx/interface';
import {RemoveTicket} from '../../../../domains/my-tickets/ngrx/actions';

@Component({
  selector: 'app-my-tickets',
  templateUrl: './my-tickets.component.html',
  styleUrls: ['./my-tickets.component.scss']
})
export class MyTicketsComponent implements OnInit {
  tickets: MyTicketData[];

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store.pipe(select(selectMyTicketsItems))
      .pipe(
        switchMap(tickets => this.store.pipe(select(selectRoomItems))
          .pipe(
            map(rooms => {
              return {
                tickets,
                rooms
              };
            })
          )),
        switchMap((data) => this.store.pipe(select(selectMoviesItems))
          .pipe(
            map(movies => {
              return {
                ...data,
                movies
              };
            })
          ))
      )
      .subscribe((data: {movies, rooms, tickets}) => {
        const {tickets, rooms, movies} = data;

        if (tickets && rooms && movies) {
          const newTickets: MyTicketData[] = tickets.map(ticket => {
            const myTicket = {
              movie: movies.find(movie => movie.id === ticket.movieId),
              room: rooms.find(room => room.id === ticket.roomId),
              rowId: ticket.rowId,
              seatId: ticket.seatId
            };

            return myTicket;
          });

          this.tickets = newTickets;
        }
      });
  }

  handleDeleteTicket(ticket: MyTicketData): void {
    const data: MyTicket = {
      roomId: ticket.room.id,
      rowId: ticket.rowId,
      movieId: ticket.movie.id,
      seatId: ticket.seatId
    };

    this.store.dispatch(new RemoveTicket(data));
  }

}
