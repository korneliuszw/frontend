import {Component, Input, OnInit} from '@angular/core';
import {Seat} from '../../../../../../../../domains/room/interfaces/seat';

@Component({
  selector: 'app-seat',
  templateUrl: './seat.component.html',
  styleUrls: ['./seat.component.scss']
})
export class SeatComponent implements OnInit {

  @Input() seat: Seat;

  constructor() { }

  ngOnInit(): void {
  }

}
