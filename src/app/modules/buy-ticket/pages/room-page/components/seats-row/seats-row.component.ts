import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SeatRow} from '../../../../../../domains/room/interfaces/seat-row';
import {Seat} from '../../../../../../domains/room/interfaces/seat';

@Component({
  selector: 'app-seats-row',
  templateUrl: './seats-row.component.html',
  styleUrls: ['./seats-row.component.scss']
})
export class SeatsRowComponent implements OnInit {

  @Input() row: SeatRow;

  @Output() onBookSeat = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  handleBookSeat(seat: Seat): void {
    const data = {
      rowId: this.row.id,
      seat
    };

    this.onBookSeat.emit(data);
  }

}
