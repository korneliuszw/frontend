import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AppState} from '../../../../ngrx/app.state';
import {select, Store} from '@ngrx/store';
import {selectRoom,} from '../../../../domains/room/ngrx/selectors';
import {Room} from '../../../../domains/room/interfaces/room';
import {map, switchMap, tap} from 'rxjs/operators';
import {MyTicket} from '../../../../domains/my-tickets/ngrx/interface';
import {StartAddingTicket} from '../../../../domains/my-tickets/ngrx/actions';
import {selectMovie} from '../../../../domains/movie/ngrx/selectors';
import {Movie} from '../../../../domains/movie/interfaces/movie';

@Component({
  selector: 'app-room-page',
  templateUrl: './room-page.component.html',
  styleUrls: ['./room-page.component.scss']
})
export class RoomPageComponent implements OnInit {
  room: Room;
  movie: Movie;

  private movieId: number;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(
        tap(params => this.movieId = +params['movieId']),
        map(params => +params['roomId']),
        switchMap(roomId => this.store.pipe(select(selectRoom, roomId))),
        switchMap(room => this.store.pipe(select(selectMovie, this.movieId))
          .pipe(
            map(movie => {
              return {
                room,
                movie
              };
            })
          ))
      )
      .subscribe((data) => {
        const {room, movie} = data;

        this.room = room;
        this.movie = movie;
      });
  }

  handleBookSeat(data): void {
    const myTicket: MyTicket = {
      movieId: this.movieId,
      roomId: this.room.id,
      rowId: data.rowId,
      seatId: data.seat.id
    };

    this.store.dispatch(new StartAddingTicket(myTicket));
  }
}
