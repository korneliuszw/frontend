import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RoomPageComponent} from './pages/room-page/room-page.component';
import {RoomDomainModule} from '../../domains/room/room-domain.module';
import {Store} from '@ngrx/store';
import {AppState} from '../../ngrx/app.state';
import {LoadRooms} from '../../domains/room/ngrx/actions';
import { SeatsRowComponent } from './pages/room-page/components/seats-row/seats-row.component';
import { SeatComponent } from './pages/room-page/components/seats-row/components/seat/seat.component';

const routes: Routes = [
  {
    path: ':movieId/:roomId',
    component: RoomPageComponent
  }
];

@NgModule({
  declarations: [RoomPageComponent, SeatsRowComponent, SeatComponent],
  imports: [
    CommonModule,
    RoomDomainModule,
    RouterModule.forChild(routes),
  ]
})
export class BuyTicketModule {
  constructor(
    private store: Store<AppState>
  ) {

    this.store.dispatch(new LoadRooms());

  }
}
