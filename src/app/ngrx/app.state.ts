import {MoviesState} from '../domains/movie/ngrx/interface';
import {RepertoireState} from '../domains/repertoire/ngrx/interface';
import {RoomsState} from '../domains/room/ngrx/interface';
import {MyTicketsState} from '../domains/my-tickets/ngrx/interface';

export interface AppState {
  movies: MoviesState;
  repertoire: RepertoireState;
  rooms: RoomsState;
  myTickets: MyTicketsState;
}
