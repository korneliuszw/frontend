import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from './ngrx/app.state';
import {LoadRepertoire} from './domains/repertoire/ngrx/actions';
import {LoadMovies} from './domains/movie/ngrx/actions';
import {LoadRooms} from './domains/room/ngrx/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'KinoZloteLwy';

  constructor(
    private store: Store<AppState>
  ) {
  }

  ngOnInit() {
    // this.store.dispatch(new LoadRooms());
    this.store.dispatch(new LoadMovies());
    this.store.dispatch(new LoadRepertoire());
  }
}
