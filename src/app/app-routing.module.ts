import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: 'buy-ticket',
  loadChildren: () => import('./modules/buy-ticket/buy-ticket.module').then(m => m.BuyTicketModule)
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
