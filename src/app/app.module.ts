import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MoviesListModule} from './modules/movies-list/movies-list.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {RepertoireModule} from './modules/repertoire/repertoire.module';
import {MyTicketsModule} from './modules/my-tickets/my-tickets.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MoviesListModule,
    RepertoireModule,
    MyTicketsModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    !environment.production
      ? StoreDevtoolsModule.instrument({
        maxAge: 25,
        logOnly: environment.production
      })
      : []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
