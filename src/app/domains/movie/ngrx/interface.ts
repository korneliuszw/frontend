import { Movie } from '../interfaces/movie';

export interface MoviesState {
    items: Movie[];
}
