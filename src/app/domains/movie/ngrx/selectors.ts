import {AppState} from '../../../ngrx/app.state';
import {createSelector} from '@ngrx/store';
import {MoviesState} from './interface';
import {Movie} from '../interfaces/movie';

export const selectMovies = (state: AppState) => state.movies;

export const selectMoviesItems = createSelector(
  selectMovies,
  (state: MoviesState) => state.items
);

export const selectMovie = createSelector(
  selectMoviesItems,
  (movies: Movie[], movieId) => movies.find(movie => movie.id === movieId)
);
