import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';
import {ACTION_KEY_START_LOAD_MOVIES, LoadMoviesSuccess} from './actions';
import {MovieClient} from '../client/movie';

@Injectable()
export class MovieEffects {
  constructor(
    private actions$: Actions,
    private client: MovieClient
  ) {
  }

  @Effect()
  loadMovies$ = this.actions$
    .pipe(
      ofType(ACTION_KEY_START_LOAD_MOVIES),
      switchMap(() => this.client.loadMovies().pipe(
        map(data => new LoadMoviesSuccess(data))
      ))
    );

}