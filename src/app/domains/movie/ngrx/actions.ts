import {Action} from '@ngrx/store';
import {Movie} from '../interfaces/movie';

export const ACTION_KEY_START_LOAD_MOVIES = '[Movies] Load all';
export const ACTION_KEY_LOAD_MOVIES_SUCCESS = '[Movies] Load success';

export class LoadMovies implements Action {
  readonly type = ACTION_KEY_START_LOAD_MOVIES;
}

export class LoadMoviesSuccess implements Action {
  readonly type = ACTION_KEY_LOAD_MOVIES_SUCCESS;

  constructor(public payload: Movie[]) {
  }
}