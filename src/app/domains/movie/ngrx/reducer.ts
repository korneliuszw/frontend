import {MoviesState} from './interface';
import {ACTION_KEY_LOAD_MOVIES_SUCCESS} from './actions';

export const initialState = {
  items: undefined
};

export function moviesReducer(
  state = initialState,
  action: any
): MoviesState {
  switch (action.type) {

    case ACTION_KEY_LOAD_MOVIES_SUCCESS:
      return {
        items: action.payload
      };

    default: {
      return state;
    }
  }
}