import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Movie} from '../interfaces/movie';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class MovieClient {

  constructor(private http: HttpClient) {
  }

  loadMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>('http://localhost:4200/assets/database/movies.json');
  }

}
