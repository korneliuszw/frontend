import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {moviesReducer} from './ngrx/reducer';
import {MovieEffects} from './ngrx/effects';
import {MovieClient} from './client/movie';



@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('movies', moviesReducer),
    EffectsModule.forFeature([MovieEffects])
  ],
  providers: [
    MovieClient
  ]
})

export class MovieDomainModule {}
