export interface Movie {
  id: number;
  title: string;
  poster: string;
  description: string;
}
