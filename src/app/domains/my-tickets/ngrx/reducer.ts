import {MyTicket, MyTicketsState} from './interface';
import {ACTION_KEY_ADD_TICKET, ACTION_KEY_REMOVE_TICKET} from './actions';
import {ActionWithPayload} from '../../../ngrx/interface';

export const initialState = {
  items: []
};

export function myTicketsReducer(
  state = initialState,
  action: ActionWithPayload
): MyTicketsState {
  switch (action.type) {

    case ACTION_KEY_ADD_TICKET:
      return {
        ...state,
        items: addTicket(state.items, action.payload)
      };

    case ACTION_KEY_REMOVE_TICKET:
      return {
        ...state,
        items: removeTicket(state.items, action.payload)
      };

    default: {
      return state;
    }
  }
}

function addTicket(myTickets: MyTicket[], newTicket: MyTicket): MyTicket[] {
  const newTickets = [
    ...myTickets
  ];

  newTickets.push(newTicket);

  return newTickets;
}

function removeTicket(myTickets: MyTicket[], myTicket: MyTicket): MyTicket[] {
  const newTickets = [
    ...myTickets
  ];

  const index = newTickets.findIndex(el => el.movieId === myTicket.movieId && el.roomId === myTicket.roomId && el.rowId === myTicket.rowId && el.seatId === myTicket.seatId);

  newTickets.splice(index, 1);

  return newTickets;
}
