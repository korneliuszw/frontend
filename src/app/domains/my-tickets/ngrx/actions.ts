import {MyTicket} from './interface';
import {ActionWithPayload} from '../../../ngrx/interface';

export const ACTION_KEY_START_ADDING_TICKET = '[My Ticket] Start adding ticket';
export const ACTION_KEY_ADD_TICKET = '[My Ticket] Add ticket';
export const ACTION_KEY_REMOVE_TICKET = '[My Ticket] Remove ticket';

export class StartAddingTicket implements ActionWithPayload {
  readonly type = ACTION_KEY_START_ADDING_TICKET;

  constructor(public payload: MyTicket) {
  }
}

export class AddTicket implements ActionWithPayload {
  readonly type = ACTION_KEY_ADD_TICKET;

  constructor(public payload: MyTicket) {
  }
}

export class RemoveTicket implements ActionWithPayload {
  readonly type = ACTION_KEY_REMOVE_TICKET;

  constructor(public payload: MyTicket) {
  }
}

