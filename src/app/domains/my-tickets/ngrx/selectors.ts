import {AppState} from '../../../ngrx/app.state';
import {createSelector} from '@ngrx/store';
import {MyTicketsState} from './interface';

export const selectMyTickets = (state: AppState) => state.myTickets;

export const selectMyTicketsItems = createSelector(
  selectMyTickets,
  (state: MyTicketsState) => state.items
);
