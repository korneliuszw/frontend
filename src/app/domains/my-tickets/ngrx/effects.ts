import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap, take} from 'rxjs/operators';
import {ACTION_KEY_START_ADDING_TICKET, AddTicket, RemoveTicket} from './actions';
import {ActionWithPayload} from '../../../ngrx/interface';
import {AppState} from '../../../ngrx/app.state';
import {select, Store} from '@ngrx/store';
import {selectMyTicketsItems} from './selectors';

@Injectable()
export class MyTicketEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>
  ) {
  }

  @Effect()
  startAddingTicket$ = this.actions$
    .pipe(
      ofType(ACTION_KEY_START_ADDING_TICKET),
      map((action: ActionWithPayload) => action.payload),
      switchMap((ticket) => this.store.pipe(select(selectMyTicketsItems), take(1)).pipe(
        map(myTickets => {
          return {
            myTickets,
            ticket
          };
        })
      )),
      map(({myTickets, ticket}) => {
        if (myTickets.find(el => el.movieId === ticket.movieId && el.roomId === ticket.roomId && el.rowId === ticket.rowId && el.seatId === ticket.seatId)) {
          return new RemoveTicket(ticket);
        }

        return new AddTicket(ticket);
      })
    );
}
