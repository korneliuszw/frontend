import {Movie} from '../../movie/interfaces/movie';
import {Room} from '../../room/interfaces/room';

export interface MyTicketsState {
  items: MyTicket[];
}

export interface MyTicket {
  roomId: number;
  rowId: number;
  seatId: number;
  movieId: number;
}

export interface MyTicketData {
  movie: Movie;
  room: Room;
  rowId: number;
  seatId: number;
}
