import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {myTicketsReducer} from './ngrx/reducer';
import {MyTicketEffects} from './ngrx/effects';
import {EffectsModule} from '@ngrx/effects';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('myTickets', myTicketsReducer),
    EffectsModule.forFeature([MyTicketEffects])
  ]
})
export class MyTicketsDomainModule { }
