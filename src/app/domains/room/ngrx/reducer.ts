import {RoomsState} from './interface';
import {ACTION_KEY_LOAD_ROOMS_SUCCESS} from './actions';
import {ActionWithPayload} from '../../../ngrx/interface';

export const initialState = {
  items: undefined
};

export function roomsReducer(
  state = initialState,
  action: ActionWithPayload
): RoomsState {
  switch (action.type) {

    case ACTION_KEY_LOAD_ROOMS_SUCCESS:
      return {
        items: action.payload
      };

    default: {
      return state;
    }
  }
}
