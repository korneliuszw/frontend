import {Room} from '../interfaces/room';

export interface RoomsState {
    items: Room[];
}
