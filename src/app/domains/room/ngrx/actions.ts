import {Action} from '@ngrx/store';
import {Room} from '../interfaces/room';
import {ActionWithPayload} from '../../../ngrx/interface';

export const ACTION_KEY_START_LOAD_ROOMS = '[Rooms] Load all';
export const ACTION_KEY_LOAD_ROOMS_SUCCESS = '[Rooms] Load success';

export class LoadRooms implements Action {
  readonly type = ACTION_KEY_START_LOAD_ROOMS;
}

export class LoadRoomsSuccess implements ActionWithPayload {
  readonly type = ACTION_KEY_LOAD_ROOMS_SUCCESS;

  constructor(public payload: Room[]) {
  }
}
