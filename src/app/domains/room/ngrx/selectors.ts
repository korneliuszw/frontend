import {AppState} from '../../../ngrx/app.state';
import {createSelector} from '@ngrx/store';
import {RoomsState} from './interface';
import {selectMyTicketsItems} from '../../my-tickets/ngrx/selectors';
import {Room} from '../interfaces/room';
import {MyTicket} from '../../my-tickets/ngrx/interface';

export const selectRooms = (state: AppState) => state.rooms;

export const selectRoomItems = createSelector(
  selectRooms,
  (state: RoomsState) => state && state.items
);

export const selectRoom = createSelector(
  selectRoomItems,
  selectMyTicketsItems,
  (rooms, myTickets: MyTicket[], roomId) => {
    if (rooms) {
      const room: Room = rooms.find(el => el.id === roomId);

      const myTicketsForThisRoom = myTickets.filter(ticket => ticket.roomId === room.id);

      const newSeatsRows = room.seatsRows.map(row => {
        const myTicketsForThisRow: MyTicket[] = myTicketsForThisRoom.filter(ticket => ticket.rowId === row.id);

        const newSeats = row.seats.map(seat => {
          const mySeat = myTicketsForThisRow.find(ticket => ticket.seatId === seat.id);

          return {
            ...seat,
            free: !mySeat
          };
        });

        return {
          ...row,
          seats: newSeats
        };

      });

      return {
        ...room,
        seatsRows: newSeatsRows
      };
    }

    return null;
  }
);
