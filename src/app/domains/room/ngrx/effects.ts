import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';
import {ACTION_KEY_START_LOAD_ROOMS, LoadRoomsSuccess} from './actions';
import {RoomClient} from '../client/room';

@Injectable()
export class RoomsEffects {
  constructor(
    private actions$: Actions,
    private client: RoomClient
  ) {
  }

  @Effect()
  loadMovies$ = this.actions$
    .pipe(
      ofType(ACTION_KEY_START_LOAD_ROOMS),
      switchMap(() => this.client.loadRooms().pipe(
        map(data => new LoadRoomsSuccess(data))
      ))
    );

}