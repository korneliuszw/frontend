import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {roomsReducer} from './ngrx/reducer';
import {RoomsEffects} from './ngrx/effects';
import {RoomClient} from './client/room';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('rooms', roomsReducer),
    EffectsModule.forFeature([RoomsEffects])
  ],
  providers: [
    RoomClient
  ]
})

export class RoomDomainModule {}
