import {Seat} from './seat';

export interface SeatRow {
  id: number;
  seats: Seat[];
}
