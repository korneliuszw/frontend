export interface Seat {
  id: number;
  free: boolean;
}
