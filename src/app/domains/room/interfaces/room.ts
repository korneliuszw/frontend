import {SeatRow} from './seat-row';

export interface Room {
  id: number;
  name: string;
  seatsRows: SeatRow[];
}
