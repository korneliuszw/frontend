import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Room} from '../interfaces/room';

@Injectable()
export class RoomClient {

  constructor(private http: HttpClient) {
  }

  loadRooms(): Observable<Room[]> {
    return this.http.get<Room[]>('http://localhost:4200/assets/database/rooms.json');
  }

}
