import {Movie} from '../../movie/interfaces/movie';

export interface RepertoireItem {
  id: number;
  movieId: number;
  roomId: number;
  time: string;
}

export interface RepertoireMovieData {
  movie: Movie;
  roomId: number;
  time: string;
}
