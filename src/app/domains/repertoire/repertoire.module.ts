import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RepertoireClient} from './client/repertoire';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {RepertoireEffects} from './ngrx/effects';
import {repertoireReducer} from './ngrx/reducer';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('repertoire', repertoireReducer),
    EffectsModule.forFeature([RepertoireEffects])
  ],
  providers: [RepertoireClient]
})
export class RepertoireDomainModule { }
