import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {RepertoireItem} from '../interfaces/repertoire';

@Injectable()
export class RepertoireClient {

  constructor(private http: HttpClient) {
  }

  loadRepertoire(): Observable<RepertoireItem[]> {
    return this.http.get<RepertoireItem[]>('http://localhost:4200/assets/database/repertoire.json');
  }

}
