import {RepertoireItem} from '../interfaces/repertoire';

export interface RepertoireState {
    items: RepertoireItem[];
}
