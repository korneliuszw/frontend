import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';
import {RepertoireClient} from '../client/repertoire';
import {ACTION_KEY_START_LOAD_REPERTOIRE, LoadRepertoireSuccess} from './actions';

@Injectable()
export class RepertoireEffects {
  constructor(
    private actions$: Actions,
    private client: RepertoireClient
  ) {
  }

  @Effect()
  loadMovies$ = this.actions$
    .pipe(
      ofType(ACTION_KEY_START_LOAD_REPERTOIRE),
      switchMap(() => this.client.loadRepertoire().pipe(
        map(data => new LoadRepertoireSuccess(data))
      ))
    );

}