import {AppState} from '../../../ngrx/app.state';
import {createSelector} from '@ngrx/store';
import {RepertoireState} from './interface';
import {selectMoviesItems} from '../../movie/ngrx/selectors';
import {RepertoireMovieData} from '../interfaces/repertoire';
import {Movie} from '../../movie/interfaces/movie';

export const selectRepertoire = (state: AppState) => state.repertoire;

export const selectRepertoireItems = createSelector(
  selectRepertoire,
  selectMoviesItems,
  (repertoire: RepertoireState, movies: Movie[]) => {
    if (repertoire.items && movies) {
      const repertoireData: RepertoireMovieData[] = [];

      repertoire.items.forEach(item => {
        const movie = movies.find(el => el.id === item.movieId);

        const data: RepertoireMovieData = {
          movie,
          roomId: item.roomId,
          time: item.time
        };

        repertoireData.push(data);
      });
console.log('repertoireData', repertoireData)
      return repertoireData;
    }
  }
);
