import {RepertoireState} from './interface';
import {ACTION_KEY_LOAD_REPERTOIRE_SUCCESS} from './actions';
import {ActionWithPayload} from '../../../ngrx/interface';

export const initialState = {
  items: undefined
};

export function repertoireReducer(
  state = initialState,
  action: ActionWithPayload
): RepertoireState {
  switch (action.type) {

    case ACTION_KEY_LOAD_REPERTOIRE_SUCCESS:
      return {
        items: action.payload
      };

    default: {
      return state;
    }
  }
}
