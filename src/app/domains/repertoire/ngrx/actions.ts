import {Action} from '@ngrx/store';
import {RepertoireItem} from '../interfaces/repertoire';
import {ActionWithPayload} from '../../../ngrx/interface';

export const ACTION_KEY_START_LOAD_REPERTOIRE = '[Repertoire] Load all';
export const ACTION_KEY_LOAD_REPERTOIRE_SUCCESS = '[Repertoire] Load success';

export class LoadRepertoire implements Action {
  readonly type = ACTION_KEY_START_LOAD_REPERTOIRE;
}

export class LoadRepertoireSuccess implements ActionWithPayload {
  readonly type = ACTION_KEY_LOAD_REPERTOIRE_SUCCESS;

  constructor(public payload: RepertoireItem[]) {
  }
}